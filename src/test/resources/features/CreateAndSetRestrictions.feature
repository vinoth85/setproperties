@page
Feature: Create and Set restrictions to a confluence page

  As a Confluence user
  I want to create a page
  and set restrictions on the page
  so that only certain users can view the page


  Scenario Outline: Create and delete the page
    Given I login to the Confluence Page as "vinothmathivanan1@gmail.com" and "vinoth85"
    When I navigate to the project atlassian space
    And I create a blank page
    And I provide the title "<pageTitle>"
    And I publish the page
    Then I validate the page "<pageTitle>" has created
    And I delete the page
    Examples:
      | pageTitle |
      | TestPage  |

  Scenario: Set restrictions to an user on an existing page
    Given I login to the Confluence Page as "vinothmathivanan1@gmail.com" and "vinoth85"
    When I navigate to the project atlassian space
    And I open the page "TestPage"
    And I edit the restrictions
    Then I set the restrictions "Viewing and editing restricted" to the page for the user "vinoth"

  Scenario: Validate if the restrictions is set on the page
    Given I login to the Confluence Page as "vinothmathivanan@gmail.com" and "vinoth85"
    When I navigate to the project atlassian space
    And I open the page "TestPage"
    Then I confirm that the page is restricted



