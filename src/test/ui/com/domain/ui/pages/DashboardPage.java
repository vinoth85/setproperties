package com.domain.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage extends BasePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }


    public void navigateToSpace() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".KcgsE")));
        driver.findElement(By.xpath(".//span[@class = 'KcgsE']")).click();
        driver.findElement(By.xpath(".//span[contains(text(),'Project Atlassian')]")).click();
        //driver.findElement(By.cssSelector(".update-item .update-item-title")).findElement(By.cssSelector("a[href*='/wiki/spaces/PA']")).click();
    }

    public void assertPage() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs("Project Atlassian - Confluence"));

    }
}


