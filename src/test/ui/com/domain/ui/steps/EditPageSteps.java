package com.domain.ui.steps;

import com.domain.ui.pages.EditPage;
import com.domain.ui.pages.PublishPage;
import com.domain.ui.utility.config.ResourceManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import static org.hamcrest.MatcherAssert.assertThat;

public class EditPageSteps extends BaseSteps {

    public EditPageSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private EditPage editPage = new EditPage(driver);
    private PublishPage publishPage = new PublishPage(driver);

    @And("^I open the page \"([^\"]*)\"$")
    public void I_open_the_page(String arg1) throws Throwable {
        editPage.getavailablePageLink().click();
    }

    @And("^I edit the restrictions$")
    public void I_edit_the_restrictions() throws Throwable {
        publishPage.getActionMenuLinkElement().click();
        editPage.getRestrictionsElement().click();
        editPage.getEditDialog();

    }

    @Then("^I set the restrictions \"([^\"]*)\" to the page for the user \"([^\"]*)\"$")
    public void set_the_restrictions(String restrictionType, String userName) throws Throwable {
        editPage.selectRestrictionType(restrictionType);
        editPage.getAccessText().contains("Has no access");
        editPage.getUserNameField().sendKeys(userName);
        editPage.selectUserName(userName);
        editPage.getAddButton().click();
        editPage.selectRestrictionType();
        editPage.getApplyButton().click();
    }

    @Then("^I confirm that the page is restricted$")
    public void I_confirm_that_the_page_is_restricted() throws Throwable {
        assertThat(String.valueOf(editPage.isElementPresent()), true);
    }
}
