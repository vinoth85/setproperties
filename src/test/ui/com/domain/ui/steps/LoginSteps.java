package com.domain.ui.steps;

import com.domain.ui.pages.HomePage;
import com.domain.ui.pages.LoginPage;
import com.domain.ui.utility.config.ResourceManager;
import cucumber.api.java.en.Given;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class LoginSteps extends BaseSteps {

    public LoginSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private String homepage_url = resourceManager.getUrl("homepage_url");
    private LoginPage loginPage = new LoginPage(driver);
    HomePage homepage = new HomePage(driver);


    @Given("^I login to the Confluence Page as \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_Login_To_The_Confluence_Page(String userName, String password) throws Throwable {
        driver.get(homepage_url);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        homepage.assertPage();
        loginPage.login(userName, password);
        loginPage.assertPage();
    }


}
