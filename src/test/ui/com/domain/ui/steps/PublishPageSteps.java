package com.domain.ui.steps;

import com.domain.ui.pages.PublishPage;
import com.domain.ui.utility.config.ResourceManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import static junit.framework.TestCase.assertTrue;

public class PublishPageSteps extends BaseSteps {

    public PublishPageSteps(ResourceManager resourceManager) {
        super(resourceManager);
    }

    private PublishPage publishPage = new PublishPage(driver);


    @And("^I provide the title \"([^\"]*)\"$")
    public void i_Provide_The_Title(String pagetitle) throws Throwable {
        publishPage.getPageTitleElement().sendKeys(pagetitle);

    }

    @And("^I publish the page$")
    public void I_publish_the_page() throws Throwable {
        String expectTitle = "TestPage - Project Atlassian - Confluence";
        publishPage.getPublishButton().click();
        assertTrue(publishPage.getPublishedPageTitle().contains(expectTitle));
    }

    @Then("^I validate the page \"([^\"]*)\" has created$")
    public void I_validate_the_page_has_created(String pageTitle) throws Throwable {
        publishPage.getPageTree(pageTitle);

    }

    @And("^I delete the page$")
    public void I_delete_the_page() throws Throwable {
        String deleteTitle = "Delete Page - TestPage - Project Atlassian - Confluence";
        publishPage.getActionMenuLinkElement().click();
        publishPage.getActionRemoveElement().click();
        assertTrue(publishPage.getDeleteTitle().contains(deleteTitle));
        publishPage.getConfirmDeleteButton().click();
    }
}
